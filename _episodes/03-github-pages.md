---
title: "Hosting Pages on GitHub"
teaching: 0
exercises: 0
questions:
- "How do I publish my pages via GitHub?"
objectives:
- "publish Markdown files as HTML on the web with GitHub Pages"
keypoints:
- "GitHub serves pages generated from `.md` files on a branch specified by the user"
---

- push to a GitHub repo
- activate Pages in the repo settings
- change something in `index.md`
- go to landing page
- make a change in `index.md` and see how landing page updates
- add a second `.md`
- add minimal YAML header

{% include links.md %}
