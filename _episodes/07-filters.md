---
title: "Working with Filters"
teaching: 0
exercises: 0
questions:
- "Key question (FIXME)"
objectives:
- "First learning objective. (FIXME)"
keypoints:
- "First key point. Brief Answer to questions. (FIXME)"
---

- add date to top of post layout
  - use `date_to_long_string: "ordinal"` filter to adjust date format
- use `group_by` filter to group post listing by `category` parameter
- link to [full list of filters][filters]

{% include links.md %}
