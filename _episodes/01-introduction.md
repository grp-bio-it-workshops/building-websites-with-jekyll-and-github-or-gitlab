---
title: "Introduction"
teaching: 0
exercises: 0
questions:
- "What is static web content?"
- "Why should I use GitHub or GitLab Pages to create my website?"
objectives:
- "explain what a static site generator does."
- "choose the appropriate tool for their website/project."
keypoints:
- "A static site generator combines page-specific content with layout elements and styling information to construct individual webpages."
- "GitHub/GitLab Pages is a good choice for people who are already familiar with Git and GitHub/GitLab."
- "This approach can be used to create a relatively small website/blog on a limited budget."
---


{% include links.md %}
