---
layout: lesson
root: .  # Is the only page that doesn't follow the pattern /:path/index.html
permalink: index.html  # Is the only page that doesn't follow the pattern /:path/index.html
---

> Before following this lesson, learners should be able to:
>
> - create a repository/project on [GitHub][github] and/or [GitLab]([gitlab]).
> - clone a local copy of a project with Git, add and commit modified files, and push/pull changes between local and remote repositories.
> - execute commands in the shell
>
> If you'd like to learn any of the skills listed above,
> the [Software Carpentry][swc] lessons on
> [the Shell][swc-shell]
> and [Git][swc-git] are a good place to start.
{: .prereq }

For those already familiar with the ways that Git
and an online platform like GitHub
can help them track and compare changes to flat text files,
and collaborate with others on projects,
GitHub and GitLab Pages provide a cost-free way to
build and host webpages.
This approach is commonly used to provide documentation
on software projects,
and to create blogs and websites for
individuals and organisations already used to working with
the Git/GitHub toolset for their other projects.
However, for those taking their first steps towards building sites like this,
the process can be confusing and intimidating.
This tutorial aims to address this,
by providing a step-by-step guide to creating a collection of pages
and combining them into a coherent site using a framework called _Jekyll_.

## Target Audience

This lesson is aimed at researchers and research software engineers
who use Git and GitHub or GitLab to manage versions of their scripts/files
and want to learn how to adapt these skills to create web pages.
The target learner already knows the basics of Git
and can view the files, commits, and history of a project
on GitHub and/or GitLab.
They are comfortable writing and editing in a text editor,
and can execute commands in the shell.

## Learning Objectives

After following this lesson,
learners will be able to:

- create formatted page content with Markdown
- configure their project to build and serve pages on GitHub or GitLab
- build a coherent site with multiple pages using the Jekyll framework
- customise the layout and style of the pages on their site

{% include links.md %}
