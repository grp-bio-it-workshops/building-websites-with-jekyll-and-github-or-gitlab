# Building Websites with Jekyll and GitHub/GitLab

Lesson material, the rendered lesson is hosted [here](https://grp-bio-it-workshops.embl-community.io/building-websites-with-jekyll-and-github-or-gitlab/).

## Maintainer(s)

Current maintainer of this lesson is

* Toby Hodges

## Authors

A list of contributors to the lesson can be found in [AUTHORS](AUTHORS)

## Citation

To cite this lesson, please consult with [CITATION](CITATION)

[lesson-example]: https://carpentries.github.io/lesson-example
